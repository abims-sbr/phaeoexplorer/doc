# Accessing data

***Restricted access***

The project data can be retrieved:

- from the [Phaeoexplorer web portal](https://phaeoexplorer.sb-roscoff.fr) on the `Download`
 page or on the organism pages (see the [web portal](./portal.md) page)

- from the ABiMS server, on `/shared/projects/phaeoexplorer_download` (see [section](#from-the-abims-server) below)
  
## From the ABiMS server

### Create an account on the ABiMS platform

See section "Request an account" in the [Contact us](./support.md) page.

### Data location

The data is located on `/shared/projects/phaeoexplorer_download`

![data location](./imgs/data_location.png)

### Data transfer

Please find documentation about data transfer on the [ABiMS Cluster documentation](https://abims-sbr.gitlab.io/cluster/doc/data/data/#transfer)

#### Via SFTP (file exchange)

Download files on your computer.

![data access sftp](./imgs/data_access_sftp.png)

#### Via SSH (interactive terminal)

Directly access the ABiMS computing cluster.

![data access ssh](./imgs/data_access_ssh.png)

To be sure to have access to the project space: 
```bash
<my-login>@slurm0:~$ newgrp phaeoexplorer_rx
```

In order to be able to run your analyses on the ABiMS cluster, you need **to ask for a project workspace** at the [ABiMS Cluster Account Manager](https://my.sb-roscoff.fr/manager2).

![data access ssh project](./imgs/data_access_ssh_project.png)



