# Contact us

## Support

If you encounter any problems with the Phaeoexplorer resources or with accessing or depositing the data, 
contact [support.phaeoexplorer@sb-roscoff.fr](mailto:support.phaeoexplorer@sb-roscoff.fr).

## Request an account

If you need an account on the ABiMS platform with access to the restricted parts of the website and the data, 
please fill out the form at the [ABiMS Cluster Account Manager](https://my.sb-roscoff.fr/manager2/register)
(select `Restricted access resources: Phaeoexplorer`).

![create ABiMS account](./imgs/data_create_abims_account.png)