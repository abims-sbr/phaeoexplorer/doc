# Utilities to work with the data

This section describes some utilities that can be helpful to analyze the Phaeoexplorer data.

*Please note that, for all steps running on the ABiMS cluster, you need an account on the ABiMS platform, with access to 
the Phaeoexplorer data. See section "Request an account" in the [Contact us](./support.md) page.*

## Extract protein sequences using orthology or functional annotations

### Using [Galaxy France](https://usegalaxy.fr/)

#### Prerequisites

You need an account on [Galaxy France](https://usegalaxy.fr/).

Please contact [support.phaeoexplorer@sb-roscoff.fr](mailto:support.phaeoexplorer@sb-roscoff.fr) to request access to the Galaxy history containing the Phaeoexplorer functional data.

#### Available workflows

- Extract protein sequences based on orthology output (Orthofinder)
    - [from a unique protein ID](https://usegalaxy.fr/u/lgueguen/w/extract-orthogroup-seq-from-single-prot-id-query)
    - [from multiple protein IDs](https://usegalaxy.fr/u/lgueguen/w/extract-orthogroup-seq-from-multiple-prot-id-queries)

- Extract protein sequences of a strain using functional annotations (Interpro)
    - [from the ID of a functional domain](https://usegalaxy.fr/u/lgueguen/w/extract-prot-seq-of-a-strain-using-functional-feature-id)

#### How-to
1. Login on [Galaxy France](https://usegalaxy.fr/) 
2. Import the history
3. Import and run a workflow 

### On the [ABiMS cluster](https://abims-sbr.gitlab.io/cluster/doc/)

Slurm batch scripts to extract protein sequences using orthology or functional annotations 
are available in `/shared/projects/phaeoexplorer_download/scripts`.

## Extract protein sequences from a list of protein IDs

### On the [ABiMS cluster](https://abims-sbr.gitlab.io/cluster/doc/)

#### Prerequisites

A text file containing your query protein IDs (one per line). Example:
```
ID1
ID2
...
```

#### How-to

###### Step 1

First, concatenate all proteomes files into one file `ALL_proteins.fa`:

`cat /shared/projects/phaeoexplorer_download/04__FINAL_PROTEOMES/*_proteins.fa > ALL_proteins.fa`

*The proteomes can also be downloaded from the [Phaeoexplorer web portal](https://phaeoexplorer.sb-roscoff.fr) on the `Download`
 page (file `all_FINAL_PROTEOMES.tar.gz`) and extracted in your working folder with a right-click from your file explorer, or in command line:
 `tar -xvzf all_FINAL_PROTEOMES.tar.gz`*

###### Step 2

Then, get the sequences of your query proteins from the `ALL_proteins.fa` file with the `seqkit grep` tool, 
where `prot_list.txt` is a text file containing your query protein IDs (one per line)


`seqkit grep -n -t protein -f prot_list.txt ALL_proteins.fa > prot_list.fa`

#### Commands on the ABiMS cluster

```bash
srun --pty bash
module purge && module load seqkit
cat /shared/projects/phaeoexplorer_download/04__FINAL_PROTEOMES/*_proteins.fa > ALL_proteins.fa
seqkit grep -n -t protein -f prot_list.txt ALL_proteins.fa > prot_list.fa
```


