# BLAST servers

***Restricted access***

## General

You can access BLAST servers from the dropdown top menu `BLAST`. 

Two servers are available:

  - **Genomes**: a BLAST server to query the genomes, the predicted transcripts and proteins of 
    the Phaeoexplorer project plus additional relevant species
  - **Transcriptomes**: a BLAST server to query the de novo transcriptomes of the 
    Phaeoexplorer project

## Using the BLAST interface

### BLAST query

1. Paste query sequence(s) or drag file containing query sequence(s) in FASTA format in the text field
2. Choose one or more databases
3. Optional: set advanced parameters (see help button). Do not forget the heading dash (`-`) before any parameter!
4. Click the `BLAST` button

![blast](./imgs/blast.png)

![blast parameters](./imgs/blast_parameters.png)

### Get the results

A graphical interface is displayed with a summary of the BLAST query (queried databases, used parameters)
and the list of the hits for each query sequence.

You can see the alignment for each hit, and you can download the hits in different formats (FASTA, tabular, XML)

![blast hits](./imgs/blast_hits.png)
