# Consortium data deposition

***Restricted access***
 
## Create an account on the ABiMS platform

See section "Request an account" in the [Contact us](./support.md) page.

## Project space dedicated to data deposit

Please deposit your data in `/shared/projects/phaeoexplorer_data_deposit`.

The files deposited should include not only figures depicting the results but also legends, methodology, data tables, etc. so that we have all the information about an analysis necessary to include it in the main manuscripts. This site will also provide a means to deposit files that will be of interest to other members of the consortium, for example lists of genes or orthogroups with specific characteristics identified by one of the genome-wide analyses that can then be analysed by the experts working on specific gene families.

Please deposit your data in one of the working group folder:

- bacterial_contaminant_data
- biological_features_evolutionary_analysis
- general_genome_features
- metabolism
- microevolution_speciation
- network_analysis
- organellar_genomes
- sexual_system_evolution
- signalling_cell_biology

## Data transfer

Please find documentation about data transfer on the [ABiMS Cluster documentation](https://abims-sbr.gitlab.io/cluster/doc/data/data/#transfer)

### Via SFTP (file exchange)

Upload files from your computer with an FTP client.

![data access sftp](./imgs/data_access_sftp.png)

### Via SSH (interactive terminal)

Directly access the ABiMS computing cluster.

![data access ssh](./imgs/data_access_ssh.png)

To be sure to have access to the project space: 
```bash
<my-login>@slurm0:~$ newgrp phaeoexplorer_data_deposit
```




